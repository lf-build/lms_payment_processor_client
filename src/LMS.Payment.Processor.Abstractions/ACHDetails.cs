﻿using LendFoundry.Foundation.Date;

namespace LMS.Payment.Processor
{
    public class AchDetails
    {  
        public string LoanNumber { get; set; }
        public string LoanProductId { get; set; }
        public string StatusWorkFlowId { get; set; }
        public string AchReferenceId { get; set; }
        public string InstructionStatus { get; set; }
        public TimeBucket PaymentScheduleDate{ get; set; }
        public TimeBucket AttemptDate{ get; set; }
        public TimeBucket LastPaymentAttemptDate{ get; set; }
        public TimeBucket ReturnDate{ get; set; }
        public string BorrowerName{ get; set; }
        public string BankName{ get; set; }
        public string RoutingNumber{ get; set; }
        public int AttemptNumber{ get; set; }
        public int PaymentNumber { get; set; }
        public double InstallmentAmount { get; set; }
        public double AchAmount { get; set; }
        public string BorrowerEmail{ get; set; }
        public string BorrowerPhone{ get; set; }
        public double TotalOutstandingAmount { get; set; }
        public double PrincipalOutstandingAmount { get; set; }
        public double InterestOutstandingAmount { get; set; }
        public double FeeOutstandingAmount { get; set; }
        public double CurrentOutstandingAmount { get; set; }
        public string ReAttemptType{ get; set; }
        public string ReturnCode{ get; set; }
        public string ReturnCodeDescription { get; set; }
        public string AchPauseStatus { get; set; }
        public string AchPauseMode { get; set; }
        public string AccrualPauseStatus { get; set; }
        public string AccrualPauseMode { get; set; }
        public double PriorPrincipalOutstanding{ get; set; }
        public double PriorInterestOutstanding{ get; set; }
        public double PriorFeeOutstanding{ get; set; }
        public double PriorLoanOutstanding{ get; set; }
        public double AfterLoanOutstanding{ get; set; }
        public double AfterPrincipalOutstanding{ get; set; }
        public double AfterInterestOutstanding{ get; set; }
        public double AfterFeeOutstanding{ get; set; }
        public double CumulativeAmountReceived{ get; set; }
         public double CumulativePrincipalReceived{ get; set; }
        public double CumulativeInterestReceived{ get; set; }
        public double CumulativeFeeReceived{ get; set; }
        public string PaymentPurpose{ get; set; }
        public string PaymentMethod{ get; set; }
        public TimeBucket TransactionDate{ get; set; }
        public string ProductPortfolioType { get; set; }
        public int DPD { get; set; }
        public int DaySinceACHPaused{ get; set; }
        public int DaySinceAccrualPaused { get; set; }
        public string UserName{ get; set; }

        public string TransactionReferenceId{ get; set; }
        public string FunderId{get;set;}
        public string FunderName{get;set;}
        public string ProductName {get;set;}
    }
}