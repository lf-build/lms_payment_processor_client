﻿using System;

namespace LMS.Payment.Processor
{
    public class AttemptRequest
    {
        public string ProductId { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public ReportType ReportType{ get; set; }   
        public string ReturnCode{ get; set; }   
    }
}