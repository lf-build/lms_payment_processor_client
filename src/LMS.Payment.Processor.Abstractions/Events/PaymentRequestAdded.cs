﻿namespace LMS.Payment.Processor
{
    public class PaymentRequestAdded 
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }

        public IFunding ApplicationFunding { get; set; }

        public string ReferenceId { get; set; }

    }
}
