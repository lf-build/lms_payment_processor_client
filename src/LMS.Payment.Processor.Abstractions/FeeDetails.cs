﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
    public class FeeDetails : IFeeDetails
    {
        public double FeeAmount { get; set; }
        public string Id { get; set; }
        public string FeeName { get; set; }
    }
}
