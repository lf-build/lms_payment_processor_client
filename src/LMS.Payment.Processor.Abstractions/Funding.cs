﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;
using System.Collections.Generic;

using Newtonsoft.Json;
using LMS.LoanManagement.Abstractions;
using LMS.LoanAccounting;
using System;
using LendFoundry.Foundation.Client;

namespace LMS.Payment.Processor
{
    public class Funding : Aggregate, IFunding
    {
        public Funding()
        { }
        public Funding(IFundingRequest request)
        {
            PaymentAmount = request.PaymentAmount;
            BankAccountNumber = request.BankAccountNumber;
            BankAccountType = request.BankAccountType;
            BankRTN = request.BankRTN;
            BorrowersName = request.BorrowersName;
            RequestedDate = new TimeBucket(request.RequestedDate);
            RequestStatus = request.Status;
            PaymentScheduleDate = new TimeBucket(request.PaymentScheduleDate);
            //PaymentInstallmentNumber = request.PaymentInstallmentNumber;
            PaymentInstrumentType = request.PaymentInstrumentType;
            PrincipalAmount = request.PrincipalAmount;
            InterestAmount = request.InterestAmount;
            AdditionalInterestAmount = request.AdditionalInterestAmount;
            CheckNo = request.CheckNo;
            PaymentPurpose = request.PaymentPurpose;
            MICR = request.MICR;
            PaymentType = request.PaymentType;
            CheckDate = new TimeBucket(request.CheckDate);
            PaymentRail = request.PaymentRail;
            ACHFundingSourceId = request.ACHFundingSourceId;
            TransactionType = request.TransactionType;
            StatusUpdatedDate = new TimeBucket(request.StatusUpdatedDate);
            IsFileCreated = request.IsFileCreated;
            IsAccounted = request.IsAccounted;
            IsReSubmitted = request.IsReSubmitted;
            DiscountAmount = request.DiscountAmount;
            FileCreatedDate = new TimeBucket(request.FileCreatedDate);
            var paymentAccountDate = request.EffectiveDate == null || request.EffectiveDate.Value.DateTime == DateTime.MinValue ?  new TimeBucket(request.PaymentScheduleDate) :  new TimeBucket(request.EffectiveDate.Value);
            PaymentAccountDate = request.PaymentAccountDate == null || request.PaymentAccountDate.DateTime == DateTime.MinValue ? paymentAccountDate : new TimeBucket(request.PaymentAccountDate);
            ActualAccountDate = new TimeBucket(request.ActualAccountDate);
            UTN = request.UTN;
            UserRemarks = request.UserRemarks;
            UserReferenceNumber = request.UserReferenceNumber;
            TDSCertificateNumber=request.TDSCertificateNumber;
            DocumentId=request.DocumentId;
            PriorInterestAmount = request.PriorInterestAmount;
            
            if (request.Fees != null)
            {
                Fees = new List<IFeeDetails>();
                Fees.AddRange(request.Fees);
            }
        }

        public string PaymentPurpose { get; set; }
        public double PaymentAmount { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string LoanNumber { get; set; }
        public TimeBucket RequestedDate { get; set; }
        public TimeBucket FundingCreatedDate { get; set; }
        public PaymentType PaymentType { get; set; }
        public string RequestStatus { get; set; }
        public string ReferenceId { get; set; }
        public TimeBucket PaymentScheduleDate { get; set; }
        public PaymentInstrumentType PaymentInstrumentType { get; set; }
        public double PrincipalAmount { get; set; }
        public double InterestAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFeeDetails, FeeDetails>))]
        public List<IFeeDetails> Fees { get; set; }
        public string CheckNo { get; set; }
        public TimeBucket CheckDate { get; set; }
        public string MICR { get; set; }
        public TimeBucket InstructionCreationDate { get; set; }
        public TimeBucket StatusUpdatedDate { get; set; }
        public double AdditionalInterestAmount { get; set; }
        public int RetryCount { get; set; }
        public TimeBucket NextRetryDate { get; set; }
        public PaymentRail PaymentRail { get; set; }
        public string ACHFundingSourceId { get; set; }
        public TransactionType TransactionType { get; set; }
        public bool IsFileCreated { get; set; }
        public bool IsAccounted { get; set; }
        public bool IsReSubmitted { get; set; }
        public TimeBucket FileCreatedDate { get; set; }
        public TimeBucket PaymentAccountDate { get; set; }
        public TimeBucket ActualAccountDate { get; set; }

        public ResubmitType ResubmitType { get; set; }
        public string BorrowersAddress { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string InitiatedBy { get; set; }
        public  double DiscountAmount{ get; set; }
        public string UTN { get; set; }
        public string UserRemarks { get; set; }
        public string UserReferenceNumber { get; set; }
        public string TDSCertificateNumber { get; set; } 
        public List<string> DocumentId{ get; set; } 
        public double PriorInterestAmount { get; set; }
    }
}