﻿namespace LMS.Payment.Processor
{
    public class FundingAddFail 
    {
        public string EntityType { get; set; }
        public string EntityId { get; set; }

        public IFunding ApplicationFunding { get; set; }

        public string SyndicationName { get; set; }

        public string Response { get; set; }


    }
}
