﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.MoneyMovement.Ach.Configuration;

namespace LMS.Payment.Processor
{
    public class FundingConfiguration : IDependencyConfiguration
    {
        public List<EventConfiguration> events { get; set; }

        public int InstructionCreationDays { get; set; }

        public int NextRetryDays { get; set; }
        
        public Dictionary<string, string> Dependencies { get; set; }

        public string BankRTNIFSCValidation { get; set; }

        public string Database { get; set; }

        public string ConnectionString { get; set; }

        public Provider Provider { get; set; }

        public List<string> CSVColumnFormat { get; set; }

        public string FailedFileName { get; set; }

        public bool FailedCSVCreate { get; set; }

        public string FailedLocation { get; set; }


    }

}