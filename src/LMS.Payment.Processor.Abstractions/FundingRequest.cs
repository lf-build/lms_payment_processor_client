﻿
using LendFoundry.Foundation.Client;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.Payment.Processor
{
    public class FundingRequest : IFundingRequest
    {
        public double PaymentAmount { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string LoanApplicationNumber { get; set; }
        public DateTimeOffset PaymentScheduleDate { get; set; }
        public DateTimeOffset RequestedDate { get; set; }
        public string PaymentPurpose { get; set; }
        public string Status { get; set; }
        public int PaymentInstallmentNumber { get; set; }
        public PaymentInstrumentType PaymentInstrumentType { get; set; }
        public double PrincipalAmount { get; set; }
        public double InterestAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFeeDetails, FeeDetails>))]
        public List<IFeeDetails> Fees { get; set; }
        public string CheckNo { get; set; }
        public DateTimeOffset CheckDate { get; set; }
        public string MICR { get; set; }
        public PaymentType PaymentType { get; set; }
        public DateTimeOffset StatusUpdatedDate { get; set; }
        public double AdditionalInterestAmount { get; set; }
        public PaymentRail PaymentRail { get; set; }
        public string ACHFundingSourceId { get; set; }
        public TransactionType TransactionType { get; set; }
        public bool IsFileCreated { get; set; }
        public bool IsAccounted { get; set; }
        public bool IsReSubmitted { get; set; }
        public DateTimeOffset FileCreatedDate { get; set; }
        public DateTimeOffset PaymentAccountDate { get; set; }
        public DateTimeOffset ActualAccountDate { get; set; }
        public double DiscountAmount{ get; set; }
        public string UTN { get; set; }
        public string UserRemarks { get; set; }
        public string UserReferenceNumber { get; set; }
        public DateTimeOffset? EffectiveDate { get; set; }
        public string TDSCertificateNumber { get; set; } 
        public List<string> DocumentId{ get; set; } 
        public double PriorInterestAmount { get; set; }
      
    }
}