﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
    public interface IFeeDetails
    {
         double FeeAmount { get; set; }
         string Id { get; set; }
         string FeeName { get; set; }
    }
}
