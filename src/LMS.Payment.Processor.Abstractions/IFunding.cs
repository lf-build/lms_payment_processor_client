﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.Payment.Processor
{
    public interface IFunding : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        double PaymentAmount { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string LoanNumber { get; set; }
        TimeBucket RequestedDate { get; set; }
        PaymentType PaymentType { get; set; }
        string RequestStatus { get; set; }
        string ReferenceId { get; set; }
        TimeBucket FundingCreatedDate { get; set; }
        TimeBucket PaymentScheduleDate { get; set; }
        PaymentInstrumentType PaymentInstrumentType { get; set; }
        double PrincipalAmount { get; set; }
        double InterestAmount { get; set; }
        double AdditionalInterestAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFeeDetails, FeeDetails>))]
        List<IFeeDetails> Fees { get; set; }
        string CheckNo { get; set; }
        TimeBucket CheckDate { get; set; }
        string MICR { get; set; }
        TimeBucket InstructionCreationDate { get; set; }
        TimeBucket StatusUpdatedDate { get; set; }
        string PaymentPurpose { get; set; }
        int RetryCount { get; set; }
        TimeBucket NextRetryDate { get; set; }
        PaymentRail PaymentRail { get; set; }
        string ACHFundingSourceId { get; set; }
        TransactionType TransactionType { get; set; }
        bool IsFileCreated { get; set; }
        bool IsAccounted { get; set; }
        bool IsReSubmitted { get; set; }
        TimeBucket FileCreatedDate { get; set; }
        TimeBucket PaymentAccountDate { get; set; }
        TimeBucket ActualAccountDate { get; set; }
        ResubmitType ResubmitType { get; set; }
        string InitiatedBy { get; set; }
        double DiscountAmount{ get; set; }
        string UTN { get; set; }
        string UserRemarks { get; set; }
        string UserReferenceNumber { get; set; }
        string TDSCertificateNumber { get; set; } 
        List<string> DocumentId{ get; set; } 
        double PriorInterestAmount { get; set; }
    }
}