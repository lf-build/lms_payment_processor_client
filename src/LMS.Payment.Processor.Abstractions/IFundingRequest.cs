﻿
using LendFoundry.Foundation.Client;
using LMS.LoanAccounting;
using LMS.LoanManagement.Abstractions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LMS.Payment.Processor
{
    public interface IFundingRequest
    {
        double PaymentAmount { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string LoanApplicationNumber { get; set; }
        DateTimeOffset PaymentScheduleDate { get; set; }
        string Status { get; set; }
        DateTimeOffset RequestedDate { get; set; }
        int PaymentInstallmentNumber { get; set; }
        PaymentInstrumentType PaymentInstrumentType { get; set; }
        double PrincipalAmount { get; set; }
        double InterestAmount { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IFeeDetails, FeeDetails>))]
        List<IFeeDetails> Fees { get; set; }
        string CheckNo { get; set; }
        DateTimeOffset CheckDate { get; set; }
        string MICR { get; set; }
        PaymentType PaymentType { get; set; }
        DateTimeOffset StatusUpdatedDate { get; set; }
        string PaymentPurpose { get; set; }
        double AdditionalInterestAmount { get; set; }
        PaymentRail PaymentRail { get; set; }
        string ACHFundingSourceId { get; set; }
        TransactionType TransactionType { get; set; }
        bool IsFileCreated { get; set; }
        bool IsAccounted { get; set; }
        bool IsReSubmitted { get; set; }
        DateTimeOffset FileCreatedDate { get; set; }
        DateTimeOffset PaymentAccountDate { get; set; }
        DateTimeOffset ActualAccountDate { get; set; }
        double DiscountAmount{ get; set; }
        string UTN { get; set; }
        string UserRemarks { get; set; }
        string UserReferenceNumber { get; set; }
        DateTimeOffset? EffectiveDate { get; set; }
        string TDSCertificateNumber { get; set; } 
        List<string> DocumentId{ get; set; } 
        double PriorInterestAmount { get; set; }
    }
}