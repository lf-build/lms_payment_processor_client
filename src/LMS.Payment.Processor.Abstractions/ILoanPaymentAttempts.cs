﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LMS.Payment.Processor
{
    public interface ILoanPaymentAttempts : IAggregate
    {
        TimeBucket AttemptDate { get; set; }
        string EntityId { get; set; }
        string EntityType { get; set; }
        string FundingRequestAttemptId { get; set; }
        string LoanNumber { get; set; }
        string ProviderReferenceNumber { get; set; }
        string ReturnCode { get; set; }
        string ReturnReason { get; set; }
        TimeBucket ReturnDate { get; set; }
        TimeBucket ProviderSubmissionDate { get; set; }
        TimeBucket ProviderResponseDate { get; set; }
        string ProviderResponseStatus { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string ReferenceId { get; set; }
        string AttemptStatus { get; set; }
         bool IsFileCreated { get; set; }
         bool IsAccounted { get; set; }
         TimeBucket FileCreatedDate { get; set; }
         
         int AttemptCount { get; set; }

    }
}