﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Payment.Processor.Persistence
{
    public interface IPaymentAttemptsRepository : IRepository<ILoanPaymentAttempts>
    {
        Task<ILoanPaymentAttempts> UpdateStatus(string referenceId, IUpdateFundingAttemptStatusRequest updateRequest);
        Task<List<ILoanPaymentAttempts>> GetAttemptData(string referenceId);
        Task<ILoanPaymentAttempts> GetAttemptData(string referenceId, string providerNumber);
        Task<List<ILoanPaymentAttempts>> GetAllAttemptData(string loanNumber);
        Task<List<ILoanPaymentAttempts>> GetPaymentAttempts();
        Task<List<ILoanPaymentAttempts>> GetAttempts(AttemptRequest attemptDate);
        Task<List<ILoanPaymentAttempts>> GetReSubmitAttempts(AttemptRequest attemptDate);
         Task<List<ILoanPaymentAttempts>> GetbyReturnDate(AttemptRequest attemptDate);
    }
}