﻿using LMS.Payment.Processor.Persistence;
using LendFoundry.Security.Tokens;

namespace LMS.Payment.Processor
{
    public interface IPaymentAttemptsRepositoryFactory
    {
        IPaymentAttemptsRepository Create(ITokenReader reader);
    }
}