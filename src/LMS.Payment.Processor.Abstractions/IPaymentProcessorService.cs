﻿using LendFoundry.MoneyMovement.Ach;
using LMS.LoanAccounting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
    public interface IPaymentProcessorService
    {
        Task<IFunding> AddFundingRequest(string entityType, string entityId, IFundingRequest request);

        Task<ILoanPaymentAttempts> AddFundingRequestAttempts(string entityType, string entityId, IPaymentRequestAttempts request);

        Task<IEnumerable<IFunding>> GetFundingRequest(List<string> statuses);

        Task<IFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus status);

        Task<ILoanPaymentAttempts> UpdateAttemptStatus(string referenceId, IUpdateFundingAttemptStatusRequest updateRequest);
        Task<ILoanPaymentAttempts> UpdateAttamptStatus(string referenceId, IUpdateFundingAttemptStatusRequest updateRequest);

        Task<IFunding> ReSubmitPayment(string entityType, string entityId, IReSubmitPaymentRequest request);

        Task<List<IFunding>> GetFundingData(string entityType, string entityId);
        Task<List<ILoanPaymentAttempts>> GetAttemptData(string referenceId);
        Task UpdateFailFunding(object objectData);
        Task UpdateNOA(object objData);
        
        // will be deprecated in favor of AddManualPayments
        Task<IPayment> AddManualPayment(string entityType, string entityId, IFundingRequest request, List<string> loanPortfolioTypes);
        
        Task<List<IPayment>> AddManualPayments(string entityType, string entityId, IFundingRequest request, List<string> loanPortfolioTypes);
        
        Task<IFunding> UpdateRetryCount(string entityType, string entityId, IUpdateRetryCount updateRequest);
        Task<IFunding> UpdateNextRetryDate(string entityType, string entityId, IUpdateRetryCount updateRequest);
        Task<IFunding> MakeRefundPayment(string entityType, string entityId, IFundingRequest request);
        Task UpdateFundingSuccess(object objData);
        Task<IFunding> GetFundingByReferenceId(string referenceId);
        Task<List<IFunding>> GetInProgressPayments(string entityType, string entityId);
        void UpdateFundingData(IFunding fundingData);

        Task<List<IFunding>> GetFundingRequestsByDate(SearchRequest requestdata);
        Task<List<IFunding>> GetFundingAccountedByDate(SearchRequest requestdata);
        Task<List<ILoanPaymentAttempts>> GetPaymentAttempts();
        Task<List<IFunding>> GetFundingAttemptsByReferenceIds(PaymentDetailsRequest paymentDetailsRequest);
        Task<List<AchDetails>> GetAttampts(AttemptRequest attemptRequest);
        Task<List<AchDetails>> GetAttempts(AttemptRequest attemptRequest);
        Task<List<LoanEarningDetails>> GetEaringReport(LoanEarningRequest loanEarningRequest);
        Task<List<LoanEarningDetails>> GetEarningReport(LoanEarningRequest loanEarningRequest);
        Task<List<AchDetails>> GetACHFailureReport(AttemptRequest attemptRequest);
        Task<List<AchDetails>> GetPaymentReport(AttemptRequest attemptRequest);
        Task<List<TDSReportDetails>> GetTDSReport(string entityType,string tagName);

    }
}