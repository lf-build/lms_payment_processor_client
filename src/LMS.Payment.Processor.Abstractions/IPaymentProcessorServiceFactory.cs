﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LMS.Payment.Processor
{
    public interface IPaymentProcessorServiceFactory
    {
        IPaymentProcessorService Create(ITokenReader reader, ITokenHandler handler,ILogger logger);
    }
}
