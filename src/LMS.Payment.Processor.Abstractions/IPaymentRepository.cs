﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Payment.Processor.Persistence
{
    public interface IPaymentRepository : IRepository<IFunding>
    {
        Task<IEnumerable<IFunding>> GetFundingRequest(IEnumerable<string> statuses);

        Task<IFunding> UpdateFundingStatus(string entityType, string entityId, DateTimeOffset statusUpdatedDate, IUpdateFundingStatus updateRequest);

        Task<IFunding> UpdateFunding(IFunding updateRequest);

        Task<List<IFunding>> GetFundingRequest(string entityType, string entityId);
        Task<IFunding> GetFundingByReferenceId(string referenceId);
        Task<IFunding> UpdateRetryCount(string entityType, string entityId, IUpdateRetryCount updateRequest);
        Task<IFunding> UpdateNextRetryDate(string entityType, string entityId, string referenceId, DateTimeOffset nextRetryDate);
        Task<List<IFunding>> GetFundingRequestsByDate(DateTime startDate, DateTime? endDate);
        Task<List<IFunding>> GetFundingAccountedByDate(DateTime startDate, DateTime? endDate);
        Task<List<IFunding>> GetFundingAttemptsByReferenceIds(List<string> referenceNumbers);

    }
}