﻿using System;

namespace LMS.Payment.Processor
{
    public interface IPaymentRequestAttempts
    {
        DateTimeOffset AttemptDate { get; set; }
        string FundingRequestAttemptId { get; set; }
        string FundingRequestID { get; set; }
        string LoanNumber { get; set; }
        string ReturnCode { get; set; }
        string ReturnReason { get; set; }
        DateTimeOffset ReturnDate { get; set; }
        DateTimeOffset ProviderResponseDate { get; set; }
        string ProviderResponseStatus { get; set; }
        string BankAccountNumber { get; set; }
        string BankAccountType { get; set; }
        string BankRTN { get; set; }
        string BorrowersName { get; set; }
        string ReferenceId { get; set; }
        string ProviderReferenceNumber { get; set; }
        DateTimeOffset ProviderSubmissionDate { get; set; }
        string AttemptStatus { get; set; }
         bool IsFileCreated { get; set; }
         bool IsAccounted { get; set; }
         DateTimeOffset FileCreatedDate { get; set; }
         int AttemptCount { get; set; }

    }
}