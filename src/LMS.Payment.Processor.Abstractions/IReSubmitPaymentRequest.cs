﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
   public interface IReSubmitPaymentRequest
    {
         string PaymentRequestId { get; set; }
         string PaymentInstrumentId { get; set; }
    }
}
