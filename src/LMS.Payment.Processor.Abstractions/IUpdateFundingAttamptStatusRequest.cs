﻿using System;

namespace LMS.Payment.Processor
{
    public interface IUpdateFundingAttemptStatusRequest
    {
        string ReferenceId { get; set; }
        string Status { get; set; }
        DateTimeOffset ResponseDate { get; set; }
        string ReturnCode { get; set; }
        string ReturnReason { get; set; }
        DateTimeOffset ProviderResponseDate { get; set; }
        string ProviderResponseStatus { get; set; }
        bool IsAccounted { get; set; }
    }
}