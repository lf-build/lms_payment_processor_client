﻿using LendFoundry.Foundation.Date;

namespace LMS.Payment.Processor
{
    public interface IUpdateFundingStatus
    {
        string ReferenceId { get; set; }

        string status { get; set; }


    }
}