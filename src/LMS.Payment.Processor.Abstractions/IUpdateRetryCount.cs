﻿using LendFoundry.Foundation.Date;
using System;

namespace LMS.Payment.Processor
{
    public interface IUpdateRetryCount
    {
        string ReferenceId { get; set; }
        int Count { get; set; }
        DateTimeOffset nextRetryDate { get; set; }

    }
}