﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LMS.Foundation.Amortization;

namespace LMS.Payment.Processor
{
    public class LoanEarningDetails
    { 
        public string LoanNumber { get; set; }
        public string LoanProductId { get; set; }
        public string StatusWorkFlowId { get; set; }
        public string BorrowerName{ get; set; }
        public double FundedAmount { get; set; }
        public double RepayAmount { get; set; }
        public TimeBucket FundedDate{ get; set; }       
        public double InstallmentAmount { get; set; }
        public int TotalNoOfPayments { get; set; }
        public double TotalAmountReceived { get; set; }
        public double TotalAmountReceivedWithoutTax { get; set; }
        public double TotalEarning { get; set; }
        public int NumberOfPaymentMade { get; set; }
        public double PercentagePaidOff { get; set; }
        public int NumberOfPaymentRemaining { get; set; }
        public double TotalOutstandingAmount { get; set; }
        public double PercentageOutstanding { get; set; }
        public string AutoPayPause { get; set; }
        public string AccrualPause { get; set; }
        public TimeBucket LastPaymentReceivedDate { get; set; }
        public double LastPaymentReceivedAmount { get; set; }
        public int DPD { get; set; }
        public double InterestAccrued { get; set; }
        public double InterestReceived { get; set; }
        public double TotalFeeReceived { get; set; }
        public double TotalFeesReceivedWithOrigination { get; set; }
        public string LoanStatus { get; set; }
        public string ProductPortfolioType { get; set; }
        public string FunderId{get;set;}
        public string FunderName{get;set;}
        public string ProductName {get;set;}
        public int Tenure { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public double LoanAmount { get; set; }
        public string BorrowerId { get; set; }
        public double TotalPrincipalReceived { get; set; }
        public double InterestEarned { get; set; }

        public double FeeEarned { get; set; }
        public double TotalDrawdownAmount { get; set; }

    }
}