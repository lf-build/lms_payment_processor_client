﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
    public class LoanEarningRequest
    {
        public string ProductId { get; set; }
        public DateTimeOffset FromDate { get; set; }
        public DateTimeOffset ToDate { get; set; }
        public DateTimeOffset? FundedFromDate { get; set; }
        public DateTimeOffset? FundedToDate { get; set; }
    }
}
