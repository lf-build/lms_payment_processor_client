﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LMS.Payment.Processor
{
    public class LoanFundingRequestAttempts : Aggregate, ILoanPaymentAttempts
    {
        public LoanFundingRequestAttempts() { }
        public LoanFundingRequestAttempts(IPaymentRequestAttempts request)
        {
            AttemptDate = new TimeBucket(request.AttemptDate);
            ReturnCode = request.ReturnCode;
            ReturnDate = new TimeBucket(request.ReturnDate);
            ProviderResponseDate = new TimeBucket(request.ProviderResponseDate);
            ProviderReferenceNumber = request.ProviderReferenceNumber;
            ProviderSubmissionDate = new TimeBucket(request.ProviderSubmissionDate);
            ProviderResponseStatus = request.ProviderResponseStatus;
            ReferenceId = request.ReferenceId;
            BankAccountNumber = request.BankAccountNumber;
            BankAccountType = request.BankAccountType;
            BankRTN = request.BankRTN;
            BorrowersName = request.BorrowersName;
            ReturnReason = request.ReturnReason;
            AttemptStatus = request.AttemptStatus;
            IsAccounted = request.IsAccounted;
            FileCreatedDate = new TimeBucket(request.FileCreatedDate);
            IsFileCreated = request.IsFileCreated;
            AttemptCount = request.AttemptCount;
        }

        public TimeBucket AttemptDate { get; set; }
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string FundingRequestAttemptId { get; set; }
        public string LoanNumber { get; set; }
        public string ProviderReferenceNumber { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnReason { get; set; }
        public TimeBucket ReturnDate { get; set; }
        public TimeBucket ProviderSubmissionDate { get; set; }
        public TimeBucket ProviderResponseDate { get; set; }
        public string ProviderResponseStatus { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string ReferenceId { get; set; }
        public string AttemptStatus { get; set; }
        public bool IsFileCreated { get; set; }
        public bool IsAccounted { get; set; }
        public TimeBucket FileCreatedDate { get; set; }
        public int AttemptCount{ get; set; }
    }
}