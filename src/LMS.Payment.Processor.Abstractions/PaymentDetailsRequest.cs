﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
    public class PaymentDetailsRequest
    {
        public List<string> ReferenceNumbers { get; set; }
    }
}
