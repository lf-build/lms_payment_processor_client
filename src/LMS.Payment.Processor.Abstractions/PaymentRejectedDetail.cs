using System;
using System.Collections.Generic;
using LendFoundry.MoneyMovement.Ach;

namespace LMS.Payment.Processor
{
    public class PaymentRejectedDetail
    {
        public PaymentRejectedDetail(Instruction instruction, string reason, string exception)
        {
            Amount = instruction.Amount;
            EntryDescription = instruction.EntryDescription;
            ExecutionDate = instruction.ExecutionDate;
            FundingSourceId = instruction.FundingSourceId;
            InternalReferenceNumber = instruction.InternalReferenceNumber;
            AccountNumber = instruction.Receiving.AccountNumber;
            AccountType = instruction.Receiving.AccountType;
            Name = instruction.Receiving.Name;
            RoutingNumber = instruction.Receiving.RoutingNumber;
            ReferenceNumber = instruction.ReferenceNumber;
            ReturnCode = instruction.ReturnCode;
            StandardEntryClassCode = instruction.StandardEntryClassCode;
            Type = instruction.Type;
            Reason = reason;
            Exception = exception;
        }

        public string ReferenceNumber { get; set; }

        public int InternalReferenceNumber { get; set; }
        
        public string ReturnCode { get; set; }
        
        public DateTimeOffset ExecutionDate { get; set; }
        
        public string Reason { get; set; }
                
        public LendFoundry.MoneyMovement.Ach.TransactionType Type { get; set; }

        public decimal Amount { get; set; }

        public string StandardEntryClassCode { get; set; }

        public string EntryDescription { get; set; }

        public string FundingSourceId { get; set; }

        public AccountType AccountType { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public string Name { get; set; }

        public string Exception { get; set; }

    }
}