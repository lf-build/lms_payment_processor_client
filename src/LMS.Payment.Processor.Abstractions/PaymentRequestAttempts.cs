﻿using LendFoundry.Foundation.Date;
using System;

namespace LMS.Payment.Processor
{
    public class PaymentRequestAttempts : IPaymentRequestAttempts
    {
        public DateTimeOffset AttemptDate { get; set; }
        public string FundingRequestAttemptId { get; set; }
        public string FundingRequestID { get; set; }
        public string LoanNumber { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnReason { get; set; }
        public DateTimeOffset ReturnDate { get; set; }
        public DateTimeOffset ProviderResponseDate { get; set; }
        public string ProviderResponseStatus { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankAccountType { get; set; }
        public string BankRTN { get; set; }
        public string BorrowersName { get; set; }
        public string ReferenceId { get; set; }
        public string ProviderReferenceNumber { get; set; }
        public DateTimeOffset ProviderSubmissionDate { get; set; }
        public string AttemptStatus{ get; set; }
        public bool IsFileCreated { get; set; }
        public bool IsAccounted { get; set; }
        public DateTimeOffset FileCreatedDate { get; set; }
        public  int AttemptCount { get; set; }


    }
}