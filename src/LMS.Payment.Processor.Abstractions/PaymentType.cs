﻿
namespace LMS.Payment.Processor
{
    public enum PaymentType
    {
       Scheduled,
       Manual,
       Additional,
       Fee,
       Refund,
       Funding
    }
}
