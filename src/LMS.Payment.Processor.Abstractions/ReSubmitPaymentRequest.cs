﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor
{
    public class ReSubmitPaymentRequest : IReSubmitPaymentRequest
    {
        public string PaymentRequestId { get; set; }
        public string PaymentInstrumentId { get; set; }

    }
}
