﻿using System;

namespace LMS.Payment.Processor
{
    public static class Settings
    {
       // public static string ServiceName { get; } = "payment-processor";
          public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "payment-processor";
    }
}