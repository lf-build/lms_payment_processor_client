﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LMS.Foundation.Amortization;

namespace LMS.Payment.Processor {

    public class TDSReportDetails {
        public string LoanNumber { get; set; }
        public string LoanProductId { get; set; }
        public string StatusWorkFlowId { get; set; }
        public string ProductPortfolioType { get; set; }
        public double LoanAmount { get; set; }
        public string BorrowerName { get; set; }
        public string BorrowerId { get; set; }
        public string BorrowerEmail { get; set; }
        public string BorrowerPhone { get; set; }
        public string FlagName { get; set; }
        public TimeBucket FlagAddedDate { get; set; }
        public string FlagAddedBy { get; set; }
        public double TotalOutstandingAmount { get; set; }
    }
}