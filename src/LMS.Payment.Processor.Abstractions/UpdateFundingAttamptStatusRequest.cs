﻿using System;

namespace LMS.Payment.Processor
{

    public class UpdateFundingAttemptStatusRequest : IUpdateFundingAttemptStatusRequest
    {
        public string ReferenceId { get; set; }
        public string Status { get; set; }
        public DateTimeOffset ResponseDate { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnReason { get; set; }
        public DateTimeOffset ProviderResponseDate { get; set; }
        public string ProviderResponseStatus { get; set; }
        public bool IsAccounted { get; set; }
    }

}
