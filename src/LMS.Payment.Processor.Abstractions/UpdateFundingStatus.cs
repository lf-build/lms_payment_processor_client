﻿using LendFoundry.Foundation.Date;
using System;

namespace LMS.Payment.Processor
{
    public class UpdateFundingStatus : IUpdateFundingStatus
    {
        public  string ReferenceId { get; set; }
        public string status { get; set; }
    }
}