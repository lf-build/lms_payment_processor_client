﻿using LendFoundry.Foundation.Date;
using System;

namespace LMS.Payment.Processor
{
    public class UpdateRetryCount : IUpdateRetryCount
    {
        public  string ReferenceId { get; set; }
        public int Count { get; set; }
        public DateTimeOffset nextRetryDate { get; set; }

    }
}