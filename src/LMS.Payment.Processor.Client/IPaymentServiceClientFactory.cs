﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Payment.Processor.Client
{
    public interface IPaymentServiceClientFactory
    {
        IPaymentProcessorService Create(ITokenReader reader);
    }
}