﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Text;
using System.Linq;
using LMS.LoanAccounting;
using LendFoundry.Foundation.Client;

namespace LMS.Payment.Processor.Client
{
    public class PaymentService : IPaymentProcessorService
    {
        #region Constructors

        public PaymentService(IServiceClient client)
        {
            Client = client;
        }

        #endregion Constructors

        #region Private Properties

        private IServiceClient Client { get; }

        #endregion Private Properties

        #region Public Methods

        public async Task<IFunding> AddFundingRequest(string entityType, string entityId, IFundingRequest request)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/add", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }

        public async Task<ILoanPaymentAttempts> AddFundingRequestAttempts(string entityType, string entityId, IPaymentRequestAttempts request)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/addfundingattempt", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<LoanFundingRequestAttempts>(objRequest);
        }

        public async Task<IEnumerable<IFunding>> GetFundingRequest(List<string> statuses)
        {
            var objRequest = new RestRequest("status", Method.GET);
            var strBuilder = new StringBuilder();
            AppendStatus(objRequest, statuses);
            return await Client.ExecuteAsync<List<Funding>>(objRequest);
        }

        public async Task<IFunding> UpdateFundingStatus(string entityType, string entityId, IUpdateFundingStatus updateRequest)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/fundingstatus", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(updateRequest);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }

        public async Task<ILoanPaymentAttempts> UpdateAttemptStatus(string referenceId, IUpdateFundingAttemptStatusRequest updateRequest)
        {
            var objRequest = new RestRequest("{referenceid}/attamptstatus", Method.POST);
            objRequest.AddUrlSegment("referenceid", referenceId);
            objRequest.AddJsonBody(updateRequest);
            return await Client.ExecuteAsync<LoanFundingRequestAttempts>(objRequest);
        }
        
        public async Task<ILoanPaymentAttempts> UpdateAttamptStatus(string referenceId,
            IUpdateFundingAttemptStatusRequest updateRequest)
        {
            return await UpdateAttemptStatus(referenceId, updateRequest);
        }

        public async Task<IFunding> ReSubmitPayment(string entityType, string entityId, IReSubmitPaymentRequest request)
        {
            var objRequest = new RestRequest("{entitytype}/{entityid}/resubmit", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }
        public async Task<List<IFunding>> GetFundingData(string entityType, string entityId)
        {
            var objRequest = new RestRequest("{entityType}/{entityId}", Method.GET);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            var response = await Client.ExecuteAsync<List<Funding>>(objRequest);
            return new List<IFunding>(response);
        }
        #endregion Public Methods

        private static void AppendStatus(IRestRequest request, IReadOnlyList<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                return;

            var url = request.Resource;
            for (var index = 0; index < statuses.Count; index++)
            {
                var tag = statuses[index];
                url = url + $"/{{name{index}}}";
                request.AddUrlSegment($"name{index}", tag);
            }
            request.Resource = url;
        }

        public async Task<List<ILoanPaymentAttempts>> GetAttemptData(string referenceId)
        {
            var objRequest = new RestRequest("fundingattempt/{referenceId}", Method.GET);
            objRequest.AddUrlSegment("referenceId", referenceId);
            var result = await Client.ExecuteAsync<List<LoanFundingRequestAttempts>>(objRequest);
            return new List<ILoanPaymentAttempts>(result);
        }

        public Task UpdateFailFunding(object objectData)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method will be deprecated in favor of AddManualPayments
        /// It is currently retained until all users switch to AddManualPayments
        /// At this time AddManualPayment is known to be called from back office UI.
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <param name="loanPortfolioTypes"></param>
        /// <returns></returns>
        public async Task<IPayment> AddManualPayment(string entityType, string entityId, IFundingRequest request, List<string> loanPortfolioTypes = null)
        {
            var objRequest = new RestRequest("/{entityType}/{entityId}/manual/payment", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<LMS.LoanAccounting.Payment>(objRequest);
        }
        
        /// <summary>
        /// This method is meant to be a superset of AddManualPayment
        /// The difference being that AddManualPayments returns a list of IPayments
        /// Whereas AddManualPayment only returns the IPayment for the last loan processed
        /// Thus behavior is exactly same in case of a single loan payment (except return type is list)
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="request"></param>
        /// <param name="loanPortfolioTypes"></param>
        /// <returns></returns>
        public async Task<List<IPayment>> AddManualPayments(string entityType, string entityId, IFundingRequest request,
            List<string> loanPortfolioTypes)
        {
            var objRequest = new RestRequest("/{entityType}/{entityId}/manual/payments", Method.POST);
            objRequest.AddUrlSegment("entityType", entityType);
            objRequest.AddUrlSegment("entityId", entityId);
            if (loanPortfolioTypes?.Any() ?? false)
            {
                objRequest.AddQueryParameter("portfolio-types", string.Join(",", loanPortfolioTypes));
            }
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<List<IPayment>>(objRequest);
        }

        public async Task<IFunding> UpdateRetryCount(string entityType, string entityId, IUpdateRetryCount updateRequest)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/retrycount", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(updateRequest);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }

        public async Task<IFunding> UpdateNextRetryDate(string entityType, string entityId, IUpdateRetryCount updateRequest)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/retrydate", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(updateRequest);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }

        public async Task<IFunding> MakeRefundPayment(string entityType, string entityId, IFundingRequest request)
        {
            var objRequest = new RestRequest("/{entitytype}/{entityid}/refund/payment", Method.POST);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            objRequest.AddJsonBody(request);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }

        public Task UpdateFundingSuccess(object objData)
        {
            throw new NotImplementedException();
        }

        public async Task<IFunding> GetFundingByReferenceId(string referenceId)
        {
            var objRequest = new RestRequest("/{referenceId}/", Method.GET);
            objRequest.AddUrlSegment("referenceId", referenceId);
            return await Client.ExecuteAsync<Funding>(objRequest);
        }

        public Task UpdateNOA(object objData)
        {
            throw new NotImplementedException();
        }

        public async Task<List<IFunding>> GetInProgressPayments(string entityType, string entityId)
        {
            var objRequest = new RestRequest("{entitytype}/{entityid}/inprogress", Method.GET);
            objRequest.AddUrlSegment("entitytype", entityType);
            objRequest.AddUrlSegment("entityid", entityId);
            var result = await Client.ExecuteAsync<List<Funding>>(objRequest);
            return new List<IFunding>(result);
        }

        public void UpdateFundingData(IFunding fundingData)
        {
            var objRequest = new RestRequest("/update/funding", Method.POST);
            objRequest.AddJsonBody(fundingData);
            Client.Execute(objRequest);
        }

        public async Task<List<IFunding>> GetFundingRequestsByDate(SearchRequest requestdata)
        {
            var request = new RestRequest("/paymentpull/date", Method.POST);

            request.AddJsonBody(requestdata);
            var fundDetails = await Client.ExecuteAsync<List<Funding>>(request);
            return new List<IFunding>(fundDetails);
        }

        public async Task<List<IFunding>> GetFundingAccountedByDate(SearchRequest requestdata)
        {
            var request = new RestRequest("/paymentaccounted/date", Method.POST);

            request.AddJsonBody(requestdata);
            var fundDetails = await Client.ExecuteAsync<List<Funding>>(request);
            return new List<IFunding>(fundDetails);
        }

        public async Task<List<ILoanPaymentAttempts>> GetPaymentAttempts()
        {
            var request = new RestRequest("/paymentattept/all", Method.GET);

            var paymentAttempts = await Client.ExecuteAsync<List<LoanFundingRequestAttempts>>(request);
            return new List<ILoanPaymentAttempts>(paymentAttempts);
        }

        public async Task<List<IFunding>> GetFundingAttemptsByReferenceIds(PaymentDetailsRequest paymentDetailsRequest)
        {
            var request = new RestRequest("/fundingattempt", Method.POST);

            request.AddJsonBody(paymentDetailsRequest);
            var fundDetails = await Client.ExecuteAsync<List<Funding>>(request);
            return new List<IFunding>(fundDetails);
        }

        public Task<List<AchDetails>> GetAttempts(AttemptRequest attemptRequest)
        {
            throw new NotImplementedException();
        }
        public Task<List<AchDetails>> GetAttampts(AttemptRequest attemptRequest)
        {
            return GetAttempts(attemptRequest);
        }

    public Task<List<LoanEarningDetails>> GetEarningReport(LoanEarningRequest loanEarningRequest)
    {
      throw new NotImplementedException();
    }
    public Task<List<LoanEarningDetails>> GetEaringReport(LoanEarningRequest loanEarningRequest)
    {
        return GetEarningReport(loanEarningRequest);
    }

    Task<List<LoanEarningDetails>> IPaymentProcessorService.GetEaringReport(LoanEarningRequest loanEarningRequest)
    {
      throw new NotImplementedException();
    }

    public Task<List<AchDetails>> GetACHFailureReport(AttemptRequest attemptRequest)
    {
      throw new NotImplementedException();
    }

    public Task<List<AchDetails>> GetPaymentReport(AttemptRequest attemptRequest)
    {
      throw new NotImplementedException();
    }

    public async Task<List<TDSReportDetails>> GetTDSReport(string entityType,string tagName)
    {
            var request = new RestRequest("{tagName}/tds/report", Method.GET);
            request.AddUrlSegment(tagName,"tagName");
            var fundDetails = await Client.ExecuteAsync<List<TDSReportDetails>>(request);
            return new List<TDSReportDetails>(fundDetails);
    }
  }
}
