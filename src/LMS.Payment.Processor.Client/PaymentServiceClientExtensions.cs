﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using System;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.Payment.Processor.Client
{
    public static class PaymentServiceClientExtensions
    {
        public static IServiceCollection AddPaymentProcessorService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPaymentServiceClientFactory>(p => new PaymentServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPaymentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPaymentProcessorService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IPaymentServiceClientFactory>(p => new PaymentServiceClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IPaymentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPaymentProcessorService(this IServiceCollection services)
        {
            services.AddSingleton<IPaymentServiceClientFactory>(p => new PaymentServiceClientFactory(p));
            services.AddSingleton(p => p.GetService<IPaymentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}