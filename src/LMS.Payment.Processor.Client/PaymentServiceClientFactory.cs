﻿using LendFoundry.Foundation.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.Payment.Processor.Client
{
    public class PaymentServiceClientFactory : IPaymentServiceClientFactory
    {
        #region Constructors

        public PaymentServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public PaymentServiceClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }


        #endregion Constructors

     

        #region Public Methods

        public IPaymentProcessorService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("payment_processor");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new PaymentService(client);
        }

        #endregion Public Methods
    }
}